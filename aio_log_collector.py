#!/usr/bin/env python

import glob
import sys
import loglib


def main():

    print("Progress:")

    loglib.get_log("orc_log.txt", ["docker", "logs", "orc"], "log dumped")
    loglib.get_log("www_log.txt", ["docker", "logs", "www"], "log dumped")
    loglib.get_log("ctl_log.txt", ["docker", "logs", "ctl"], "log dumped")
    loglib.get_log("vnfm_log.txt", ["docker", "logs", "vnfm"], "log dumped")
    loglib.get_log("vnfm-nginx_log.txt", ["docker", "logs", "vnfm-nginx"], "log dumped")
    loglib.get_log("docker_ps.txt", ["docker", "ps"], "docker ps dumped")
    loglib.get_log("ip_routes.txt", ["ip", "route", "show", "table", "all"], "ip route dumped")
    loglib.get_log("ip_address.txt", ["ip", "address"], "ip addresses dumped")
    loglib.get_log("ip_tables.txt", ["iptables-save"], "iptables dumped")
    loglib.get_log("ps.txt", ["ps"], "ps dumped")
    loglib.get_log("disk_df.txt", ["df", "-h"], "disk df dumped")


    txt_files = glob.glob('*.txt')
    loglib.archive(txt_files, "AIO_", "_logs")

    loglib.del_files(txt_files)


if __name__ == "__main__":
    sys.exit(main())
