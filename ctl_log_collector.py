#!/usr/bin/env python

import glob
import sys
import loglib


def main():

    print("Progress:")

    loglib.get_log("ctl_log.txt", ["docker", "logs", "ctl"], "log dumped")
    loglib.get_log("ctl_dbs_log.txt", ["docker", "logs", "ctl-dbs"], "log dumped")
    loglib.get_log("docker_ps.txt", ["docker", "ps"], "docker ps dumped")

    loglib.info_os()

    txt_files = glob.glob('*.txt')
    loglib.archive(txt_files, "CTL_", "_logs")

    loglib.del_files(txt_files)


if __name__ == "__main__":
    sys.exit(main())
