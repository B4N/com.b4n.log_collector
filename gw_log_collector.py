#!/usr/bin/env python

import glob
import sys
import loglib


def main():

    ovs_bridge_name = "vtep"
    openflow_version = "Openflow13"

    print("Progress:")

    loglib.get_log("flows.txt", ["ovs-ofctl", "dump-flows", ovs_bridge_name, "-O", openflow_version], "flow dumped")
    loglib.get_log("groups.txt", ["ovs-ofctl", "dump-groups", ovs_bridge_name, "-O", openflow_version], "groups dumped")
    loglib.get_log("logread.txt", ["logread"], "logread dumped")
    loglib.get_log("ovs-vswitchd-log.txt", ["cat", "/var/log/openvswitch/ovs-vswitchd.log"], "ovs-vswitchd.log dumped")
    loglib.get_log("ovs_list_controllers.txt", ["ovs-vsctl", "list", "controller"], "list controllers dumped")
    loglib.get_log("ovs_show.txt", ["ovs-vsctl", "show"], "ovs show dumped")

    loglib.info_os()

    txt_files = glob.glob('*.txt')
    loglib.archive(txt_files, "GW_", "_logs")

    loglib.del_files(txt_files)


if __name__ == "__main__":
    sys.exit(main())
