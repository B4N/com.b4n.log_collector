#!/usr/bin/env python

import sys
from jira import JIRA


def main():
    authentication()
    print(jira)

    project = str(input('Enter jira project name (ORC, CTL): '))
    subject = input('Enter issue subject: ')
    description = input('Enter issue description: ')
    attachment_name = input('Enter attachment name: ')

    create_issue(project, subject, description, attachment_name)


def authentication():
    user = 'v.mirkushin@brain4net.com'
    apikey = 'WusZOPqDyIIwxagdrxFP58BE'
    server = 'https://brain4net.atlassian.net'

    options = {
     'server': server
    }
    global jira
    jira = JIRA(options, basic_auth=(user, apikey))


def create_issue(project, subject, description, attachment_name):

    new_issue = jira.create_issue(project=project, summary=subject, description=description, issuetype={'name': 'Bug'})
    print(new_issue)
    with open(attachment_name, 'rb') as f:
        jira.add_attachment(issue=new_issue, attachment=f)


def del_issue(issue_number):
    issue = jira.issue(issue_number)
    issue.delete()


if __name__ == "__main__":
    sys.exit(main())
