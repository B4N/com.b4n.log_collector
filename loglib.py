#!/usr/bin/env python

import subprocess
import zipfile
import socket
import datetime
import glob
import sys


def get_log(file_name, command, msg):

    with open(file_name, 'w') as f:
        subprocess.call(command, stderr=subprocess.STDOUT, stdout=f)
        print(msg)


def archive(files, name_start, name_end):

    hn = socket.gethostname()
    now = str(datetime.datetime.now())
    now = now.replace(" ", "_")
    now = now.replace(":", "_")
    arch_name = str(name_start+hn+now+name_end+".zip")
    z = zipfile.ZipFile(arch_name, 'w')
    file_list = files
    for x in file_list:
        z.write(x)
    print("Archive created:")
    print(arch_name)


def del_files(files):
    file_list = files
    for x in file_list:
        subprocess.call(["rm", "-r", x])


def info_os():
    get_log("ip_routes.txt", ["ip", "route", "show", "table", "all"], "ip route dumped")
    get_log("ip_address.txt", ["ip", "address"], "ip addresses dumped")
    get_log("ip_tables.txt", ["iptables-save"], "iptables dumped")
    get_log("ps.txt", ["ps"], "ps dumped")
    get_log("disk_df.txt", ["df", "-h"], "disk df dumped")

