#!/usr/bin/env python

import subprocess
import glob
import datetime
import zipfile
import sys
import jira_case
try:
    import configparser as ConfigParser
except ImportError:
    import ConfigParser

def main():
    clear_old_zips()

    config = ConfigParser.ConfigParser()
    config.read("config.ini")

    def_pass = config.get("inventory", "def_pass")
    def_login = config.get("inventory", "def_login")

    cpe_ips = config.get("inventory", "cpe_ips").split(', ')
    gw_ips = config.get("inventory", "gw_ips").split(', ')
    ctl_ips = config.get("inventory", "ctl_ips").split(', ')

    run_remote(cpe_ips, "cpe_log_collector.py", def_pass, def_login)
    run_remote(gw_ips, "gw_log_collector.py", def_pass, def_login)
    run_remote(ctl_ips, "ctl_log_collector.py", def_pass, def_login)
    run_local("aio_log_collector.py")
    archive()
    print("done")

# jira case creation
    
    user = config.get("jira", "user")
    apikey = config.get("jira", "apikey")
    server = config.get("jira", "server")

    answer = None
    while answer not in ("yes", "no"):
       answer = raw_input("Do you want to create a case in jira? Enter yes or no: ")
       if answer == "yes":
           jira_case.authentication(user, apikey, server) 
           project = raw_input("Enter jira project name (ORC or CTL): ")
           subject = raw_input('Enter issue subject: ')
           description = raw_input('Enter issue description: ')
           attachment_name = raw_input('Enter attachment name: ')
           jira_case.create_issue(project, subject, description, attachment_name)
       elif answer == "no":
           break
       else:
           print("Please enter yes or no.")

    answer2 = None
    while answer2 not in ("yes", "no"):
       answer2 = raw_input("Do you want to attach file to existing case? Enter yes or no: ")
       if answer2 == "yes":
           jira_case.authentication(user, apikey, server)
           issue = raw_input("Enter issue name (For example ORC-2277): ")
           attachment_name = raw_input('Enter attachment name: ')
           jira_case.add_attachment(attachment_name, issue)
       elif answer2 == "no":
           break
       else:
           print("Please enter yes or no.")



def clear_old_zips():
    subprocess.check_output(["rm -f *.zip"], shell=True)
    print("Old *.zip cleared")


def run_remote(ips, script, def_pass, def_login):
    for x in ips:
        try:
            print("Start for "+x)
            subprocess.check_output(["ping", "-c", "1", x])
            print(x+" available")
            subprocess.call(["sshpass", "-p", def_pass, "scp", "./loglib/loglib.py", def_login+"@"+x+":/root"])
            print("Scrirt copied on "+x)
            subprocess.call(["sshpass", "-p", def_pass, "scp", script, def_login+"@"+x+":/root"])
            print("Scrirt copied on "+x)
            subprocess.call(["sshpass", "-p", def_pass, "ssh", def_login+"@"+x, "./"+script])
            print("Scrirt executed on "+x)
            subprocess.call(["sshpass", "-p", def_pass, "scp", def_login+"@"+x+":/root/*_logs.zip", "./"])
            subprocess.call(["sshpass", "-p", def_pass, "ssh", def_login+"@"+x, "rm", "*_logs.zip"])
        except subprocess.CalledProcessError:
            print(x+" not available by ping")


def run_local(script):
    try:
        print("Start for localhost")
        subprocess.call(["./"+script])
        print("Script executed on localhost")
    except subprocess.CalledProcessError:
        print("localhost not available")


def archive():
    files = glob.glob('*_logs.zip')
    print(files)
    now = str(datetime.datetime.now())
    now = now.replace(" ", "_")
    now = now.replace(":", "_")
    arch_name = str("ALL_LOGs"+now+".zip")
    z = zipfile.ZipFile(arch_name, 'w')
    file_list = files
    for x in file_list:
        z.write(x)
    print("Archive created:")
    print(arch_name)


if __name__ == "__main__":
    sys.exit(main())

